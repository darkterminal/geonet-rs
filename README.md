# Geographic Network Tools

Network tools that run from multiple geographic locations using the [GeoNet API](https://geonet.shodan.io).

<p>
<a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-_red.svg"></a>
<a href="https://rust-reportcard.xuri.me/report/gitlab.com/shodan-public/geonet-rs"><img src="https://rust-reportcard.xuri.me/badge/gitlab.com/shodan-public/geonet-rs"></a>
<a href="https://twitter.com/shodanhq"><img src="https://img.shields.io/twitter/follow/shodanhq.svg?logo=twitter"></a>
</p>

The following commands are currently provided:

* [geodns](#geodns): lookup DNS records for a hostname
* [geoping](#geoping): ping an IP/ hostname from multiple locations

## Installation

Grab the [latest Debian package](https://gitlab.com/shodan-public/geonet-rs/-/releases) and install all available geonet commands:

```shell
$ wget https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/0.3.0/geonet_0.3.0_amd64.deb
$ sudo dpkg -i geonet_0.3.0_amd64.deb
```

Prebuilt releases of the individual commands are also available for Linux x64. To get the most recent release from the [Release page](https://gitlab.com/shodan-public/geonet-rs/-/releases), download the command and ``chmod`` it. For example, to download the ``0.2.0`` release of ``geoping``:

```shell
$ wget https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/0.2.0/geoping
$ chmod +x geoping
```

To build the utilities from source you need to have Rust stable or nightly installed. Afterwards you can simply run:

```shell
cargo build --release
```

The tools will then be available in the ``target/release`` folder.

## geodns

Sends a DNS lookup for a hostname and DNS record type from multiple locations around the world.

[![asciicast](https://asciinema.org/a/nKthljCVqTTRGnHLZO97KkTic.svg)](https://asciinema.org/a/nKthljCVqTTRGnHLZO97KkTic)

### Usage

To see the supported options run:

```shell
geodns -h
```

The ``geodns`` command supports JSON output using the JSON-NL format which can then be consumed by tools such as ``jq``.

### Examples

Get the IPv4 addresses (``A`` records) for the ``shodan.io`` hostname:

```shell
$ geodns shodan.io
104.18.12.238                  Santa Clara
                               Clifton
                               London
                               Amsterdam
                               Singapore
                               Frankfurt am Main
                               Doddaballapura

104.18.13.238                  Santa Clara
                               Clifton
                               London
                               Amsterdam
                               Singapore
                               Frankfurt am Main
                               Doddaballapura
```

Get the IPv6 addresses (``AAAA`` records) for the ``shodan.io`` hostname:

```shell
$ geodns --type AAAA shodan.io
2606:4700::6812:cee            Santa Clara
                               Clifton
                               London
                               Singapore
                               Amsterdam
                               Frankfurt am Main
                               Doddaballapura

2606:4700::6812:dee            Santa Clara
                               Clifton
                               London
                               Singapore
                               Amsterdam
                               Frankfurt am Main
                               Doddaballapura
```

Get the mail servers (``MX`` records) for ``shodan.io`` and output the results in JSON-NL format:

```shell
$ geodns -o json -t MX shodan.io
{"answers":[{"type":"MX","value":"alt2.aspmx.l.google.com."},{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt1.aspmx.l.google.com."}],"from_loc":{"city":"Santa Clara","country":"US","latlon":"37.3924,-121.9623"}}
{"answers":[{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt2.aspmx.l.google.com."},{"type":"MX","value":"alt1.aspmx.l.google.com."}],"from_loc":{"city":"Clifton","country":"US","latlon":"40.8344,-74.1377"}}
{"answers":[{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt1.aspmx.l.google.com."},{"type":"MX","value":"alt2.aspmx.l.google.com."}],"from_loc":{"city":"London","country":"GB","latlon":"51.5085,-0.1257"}}
{"answers":[{"type":"MX","value":"alt1.aspmx.l.google.com."},{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt2.aspmx.l.google.com."}],"from_loc":{"city":"Amsterdam","country":"NL","latlon":"52.3740,4.8897"}}
{"answers":[{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt1.aspmx.l.google.com."},{"type":"MX","value":"alt2.aspmx.l.google.com."}],"from_loc":{"city":"Frankfurt am Main","country":"DE","latlon":"50.1025,8.6299"}}
{"answers":[{"type":"MX","value":"alt2.aspmx.l.google.com."},{"type":"MX","value":"alt1.aspmx.l.google.com."},{"type":"MX","value":"aspmx.l.google.com."}],"from_loc":{"city":"Singapore","country":"SG","latlon":"1.3215,103.6957"}}
{"answers":[{"type":"MX","value":"alt1.aspmx.l.google.com."},{"type":"MX","value":"aspmx.l.google.com."},{"type":"MX","value":"alt2.aspmx.l.google.com."}],"from_loc":{"city":"Doddaballapura","country":"IN","latlon":"13.2257,77.5750"}}
```

## geoping

Sends a ping request to the designated address from multiple locations around the world.

[![asciicast](https://asciinema.org/a/xc15PlEMYWp8HQuUlrYHIA8pE.svg)](https://asciinema.org/a/xc15PlEMYWp8HQuUlrYHIA8pE)

### Usage

To see the supported options run:

```sh
geoping -h
```

The ``geoping`` command supports JSON output using the JSON-NL format which can then be consumed by tools such as ``jq``.

### Examples

Get the latency for ``1.1.1.1`` from multiple locations around the world:

```console
$ geoping 1.1.1.1
Amsterdam (NL)                 2.104 ms       (min: 1.747 ms, max: 2.771 ms)
Clifton (US)                   1.867 ms       (min: 1.353 ms, max:  2.81 ms)
Doddaballapura (IN)             1.53 ms       (min: 1.192 ms, max: 1.974 ms)
Frankfurt am Main (DE)         1.332 ms       (min: 0.983 ms, max: 1.887 ms)
London (GB)                    2.593 ms       (min: 2.224 ms, max: 3.326 ms)
Santa Clara (US)               3.356 ms       (min: 2.977 ms, max: 3.946 ms)
Singapore (SG)                 1.682 ms       (min: 1.247 ms, max: 2.431 ms)
```

Get the latency for ``yahoo.com``:

```console
$ geoping yahoo.com
Amsterdam (NL)                 148.342 ms     (min: 148.302 ms, max: 148.38 ms)
Clifton (US)                   103.628 ms     (min: 101.306 ms, max: 106.027 ms)
Doddaballapura (IN)            241.13 ms      (min: 241.03 ms, max: 241.251 ms)
Frankfurt am Main (DE)         115.007 ms     (min: 114.912 ms, max: 115.065 ms)
London (GB)                    106.754 ms     (min: 106.66 ms, max: 106.834 ms)
Santa Clara (US)               62.238 ms      (min: 61.867 ms, max: 62.561 ms)
Singapore (SG)                 238.498 ms     (min: 238.407 ms, max: 238.612 ms)
```

Get the latency for ``yahoo.com`` and output the results in JSON format:

```console
$ geoping -o json yahoo.com
{"ip":"98.137.11.164","is_alive":true,"min_rtt":148.197,"avg_rtt":148.558,"max_rtt":149.104,"rtts":[149.10435,148.19693,148.37193],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Amsterdam","country":"NL","latlon":"52.3740,4.8897"}}
{"ip":"98.137.11.164","is_alive":true,"min_rtt":71.686,"avg_rtt":72.039,"max_rtt":72.382,"rtts":[72.38197,71.68579,72.04914],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Clifton","country":"US","latlon":"40.8344,-74.1377"}}
{"ip":"74.6.231.20","is_alive":true,"min_rtt":241.031,"avg_rtt":241.322,"max_rtt":241.743,"rtts":[241.74309,241.0314,241.19043],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Doddaballapura","country":"IN","latlon":"13.2257,77.5750"}}
{"ip":"74.6.231.20","is_alive":true,"min_rtt":115.003,"avg_rtt":115.327,"max_rtt":115.922,"rtts":[115.92221,115.056755,115.00311],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Frankfurt am Main","country":"DE","latlon":"50.1025,8.6299"}}
{"ip":"74.6.231.21","is_alive":true,"min_rtt":106.846,"avg_rtt":107.252,"max_rtt":107.806,"rtts":[107.806206,106.845856,107.10382],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"London","country":"GB","latlon":"51.5085,-0.1257"}}
{"ip":"74.6.143.25","is_alive":true,"min_rtt":61.551,"avg_rtt":62.389,"max_rtt":63.351,"rtts":[63.351395,62.265873,61.551094],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Santa Clara","country":"US","latlon":"37.3924,-121.9623"}}
{"ip":"74.6.231.20","is_alive":true,"min_rtt":238.492,"avg_rtt":238.777,"max_rtt":239.276,"rtts":[239.27617,238.49178,238.56306],"packets_sent":3,"packets_received":3,"packet_loss":0.0,"from_loc":{"city":"Singapore","country":"SG","latlon":"1.3215,103.6957"}}
```
